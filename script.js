

db.users.insertMany(
	[
		{
			"firstName": "Diane",
			"lastName": "Murphy",
			"Email": "dmurphy@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Mary",
			"lastName": "Patterson",
			"Email": "mpatterson@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Jeff",
			"lastName": "Firelli",
			"Email": "jfirelli@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Gerard",
			"lastName": "Bondur",
			"Email": "gbondur@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Pamela",
			"lastName": "Castillo",
			"Email": "pcastillo@mail.com",
			"isAdmin": true,
			"isActive": false
		},
		{
			"firstName": "George",
			"lastName": "Vanauf",
			"Email": "gvanauf@mail.com",
			"isAdmin": true,
			"isActive": true
		}
		]

);

db.users.find()

db.courses.insertMany(
	[
		{
			"Name": "Professional Development",
			"Price": "10000"
		},
		{
			"Name": "Business Processing",
			"Price": "13000"
		}
	]
);

db.courses.find()

db.users.find()


db.courses.updateOne(
	{
		"Name": "Professional Development"
	},
	{
		$set : "enrollees": [ ObjectId("6125b5c75ee23742b00fb2c5") ,  ObjectId("6125b5c75ee23742b00fb2c7") ]
				
	}
		
);